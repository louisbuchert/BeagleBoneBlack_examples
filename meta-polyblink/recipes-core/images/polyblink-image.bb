SUMMARY = "Polyblink image."

IMAGE_INSTALL = "packagegroup-core-boot ${CORE_IMAGE_EXTRA_INSTALL}"

IMAGE_INSTALL:append = " polyblink-mod"

IMAGE_LINGUAS = " "

LICENSE = "MIT"

inherit core-image


