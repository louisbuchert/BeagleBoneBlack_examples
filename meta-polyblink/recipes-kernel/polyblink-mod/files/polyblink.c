#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/mod_devicetable.h>
#include <linux/gpio.h>
#include <linux/pwm.h>

#define DUTY_CYCLE_NS 500000000

/**
 * Device handler
 */
struct polyblink {
	struct gpio_desc *gpio;
	struct pwm_device *pwm;
	struct device *dev;
};

/**
 * Configure GPIO
 *
 * @pdata: device handler
 *
 * Returns 0 or error
 */
static int polyblink_gpio(struct polyblink *pdata)
{
	/* managed, no need to call put function */
	pdata->gpio = devm_gpiod_get(pdata->dev, NULL, GPIOD_OUT_HIGH);
	if (IS_ERR(pdata->gpio)) {
		dev_err(pdata->dev, "Couldn't get GPIO, exiting.\n");
		return PTR_ERR(pdata->gpio);
	}
	gpiod_set_value(pdata->gpio, 1);

	return 0;
}

/**
 * Configure PWM
 *
 * @pdata: device handler
 *
 * Returns 0 or error
 */
static int polyblink_pwm(struct polyblink *pdata)
{
	struct pwm_state state;

	/* managed, no need to call put function */
	pdata->pwm = devm_pwm_get(pdata->dev, NULL);
	if (IS_ERR(pdata->pwm)) {
		dev_err(pdata->dev, "Couldn't get PWM, exiting.\n");
		return PTR_ERR(pdata->pwm);
	}
	pwm_init_state(pdata->pwm, &state);
	pwm_apply_state(pdata->pwm, &state);
	pwm_set_duty_cycle(pdata->pwm, DUTY_CYCLE_NS);
	pwm_enable(pdata->pwm);

	return 0;
}

/**
 * Platform probe
 *
 * @pdev: platform device
 *
 * Returns 0 or error
 */
static int polyblink_probe(struct platform_device *pdev)
{
	struct polyblink *pdata;
	int ret = 0;

	dev_info(&pdev->dev, "Probed!\n");
	/* managed, no need to call kfree function, doing it anyway */
	pdata = devm_kmalloc(&pdev->dev, sizeof(struct polyblink), GFP_KERNEL);
	if (!pdata) {
		dev_err(&pdev->dev, "Couldn't allocate data, exiting.\n");
		return -ENOMEM;
	}
	pdata->dev = &pdev->dev;
	platform_set_drvdata(pdev, pdata);

	ret = polyblink_gpio(pdata);
	if (ret)
		goto free_pdata;

	ret = polyblink_pwm(pdata);
	if (ret)
		goto free_pdata;

	return 0;

free_pdata:
	devm_kfree(&pdev->dev, pdata);

	return ret;
}

/**
 * Platform remove
 *
 * @pdev: platform device
 *
 * Returns 0 or error
 */
static int polyblink_remove(struct platform_device *pdev)
{
	struct polyblink *pdata = platform_get_drvdata(pdev);

	devm_kfree(&pdev->dev, pdata);
	dev_info(&pdev->dev, "Removed!\n");

	return 0;
}

static const struct of_device_id polyblink_ids[] = {
	{ .compatible = "acme,polyblink" },
	{ /* sentinel */ }
};

static struct platform_driver polyblink_driver = {
	.probe = polyblink_probe,
	.remove = polyblink_remove,
	.driver = {
		.name = KBUILD_MODNAME,
		.of_match_table = polyblink_ids,
		.owner = THIS_MODULE
	}
};

/**
 * Init module
 */
int init_module(void)
{
	pr_info("Hello World!\n");
	platform_driver_register(&polyblink_driver);
	return 0;
}

/**
 * Cleanup module
 */
void cleanup_module(void)
{
	pr_info("Goodbye Cruel World!\n");
	platform_driver_unregister(&polyblink_driver);
}

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Louis Buchert");
/* so that the init system can autolaod the module if in device tree */
MODULE_DEVICE_TABLE(of, polyblink_ids);
MODULE_DESCRIPTION("Module blinking a LED on the BeagleBone Black");

